# require 'byebug'
class Fixnum
  def in_words
    # debugger
    num = self.to_s.split("_").join("").to_i
    return "zero" if num == 0
    trillion, billion, million, thousand, hundred, under_hundreds = 0,0,0,0,0,0

    trillion = three_digit_to_word(num / 1000000000000)
    num -= (num / 1000000000000) * 1000000000000
    billion = three_digit_to_word(num / 1000000000)
    num -= (num / 1000000000) * 1000000000
    million = three_digit_to_word(num / 1000000)
    num -= (num / 1000000) * 1000000
    thousand = three_digit_to_word(num / 1000)
    num -= (num / 1000) * 1000
    under_hundreds = num

    word_array = {
      "trillion" => trillion,
      "billion" => billion,
      "million" => million,
      "thousand" => thousand}

    res_string = ""
    word_array.each do |place, num|
      if num != 0
        res_string += "#{num} #{place} "
      end
    end
    if under_hundreds > 0
      res_string += "#{three_digit_to_word(under_hundreds)}"
    else
      res_string.chop!
    end
  end

  def three_digit_to_word(num)
    return 0 if num == 0
    hundreds = 0
    tens = 0
    ones = 0
    hundreds = (num / 100) if (num / 100) > 0
    num -= hundreds * 100

    tens = num / 10 if num / 10 > 0
    num -= tens * 10

    ones = num

    if hundreds > 0 && tens > 0 && ones > 0
      "#{digit_words(hundreds)} hundred #{convert_two_digits(tens, ones)}"
    elsif hundreds > 0 && tens > 0
      "#{digit_words(hundreds)} hundred #{ten_mults(tens)}"
    elsif hundreds > 0 && ones > 0
      "#{digit_words(hundreds)} hundred #{digit_words(ones)}"
    elsif hundreds > 0
      "#{digit_words(hundreds)} hundred"
    elsif tens > 0 && ones > 0
       "#{convert_two_digits(tens, ones)}"
    elsif tens > 0
      "#{ten_mults(tens)}"
    elsif ones > 0
      "#{digit_words(ones)}"
    end
  end

  def digit_words(digit)
    if digit == 1
      "one"
    elsif digit == 2
      "two"
    elsif digit == 3
      "three"
    elsif digit == 4
      "four"
    elsif digit == 5
      "five"
    elsif digit == 6
      "six"
    elsif digit == 7
      "seven"
    elsif digit == 8
      "eight"
    elsif digit == 9
      "nine"
    end
  end

  def ten_mults(num)
    if num == 1
      "ten"
    elsif num == 2
      "twenty"
    elsif num == 3
      "thirty"
    elsif num == 4
      "forty"
    elsif num == 5
      "fifty"
    elsif num == 6
      "sixty"
    elsif num == 7
      "seventy"
    elsif num == 8
      "eighty"
    elsif num == 9
      "ninety"
    end
  end

  def convert_two_digits(tens, ones)
    if tens == 1
      if ones == 1
        "eleven"
      elsif ones == 2
        "twelve"
      elsif ones == 3
        "thirteen"
      elsif ones == 4
        "fourteen"
      elsif ones == 5
        "fifteen"
      elsif ones == 6
        "sixteen"
      elsif ones == 7
        "seventeen"
      elsif ones == 8
        "eighteen"
      elsif ones == 9
        "nineteen"
      end
    else
      "#{ten_mults(tens)} #{digit_words(ones)}"
    end
  end
end
